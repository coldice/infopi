﻿以BSD 2-Clause许可协议发布

使用教程，可参考这个博客
http://www.cnblogs.com/infopi/


infopi.odg为LibreOffice的绘图文件
infopi.ods为LibreOffice的电子表格文件


demo_cfg.zip为演示用的配置文件，第一次使用时可以把它解压到infopi目录下。
解压后，进入infopi/cfg目录，应该能看到config.ini、admin.txt、还有几个目录，这些就是演示用的配置。